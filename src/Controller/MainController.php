<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/home", name="main")
     */
    public function index(): Response
    {
        return $this->render('pages/index.html.twig');
    }

    /**
     * @Route("/displayUser", name="displayUser")
     * @return Response
     */
    public function displayUserShow(): Response
    {
        return $this->render('pages/displayUser.html.twig');
    }

    /**
     * @Route("/modifyUser", name="modifyUser")
     * @return Response
     */
    public function modifyUser()
    {
        return $this->render('pages/modifyUser.html.twig');
    }

    /**
     * @Route("/displayProfil/{id}", name="displayProfil")
     * @param $id
     * @return Response
     */
    public function displayProfil($id): Response
    {
        return $this->render('pages/displayProfile.html.twig', [
            'profilId' => $id
        ]);
    }
}
