<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class BrameController extends AbstractController
{
    /**
     * @Route("/displayBrame/{id}", name="displayBrame")
     * @param $id
     * @return Response
     */
    public function displayBrame($id): Response
    {
        return $this->render('pages/displayBrame.html.twig', [
            'postId' => $id
        ]);
    }

    /**
     * @Route("/displayHashtag/{hashtag}", name="displayHashtag")
     * @param $hashtag
     * @return Response
     */
    public function displayHashtag($hashtag): Response
    {
        return $this->render('pages/displayHashtag.html.twig', [
            'hashtag' => $hashtag
        ]);
    }

    /**
     * @Route("/newBrame", name="newBrame")
     */
    public function newBrame(): Response
    {
        return $this->render('pages/newBrame.html.twig');
    }

    /**
     * @Route("/modifyBrame/{id}", name="modifyBrame")
     */
    public function modifyBrame($id)
    {
        return $this->render('pages/modifyBrame.html.twig', [
            'postId' => $id
        ]);
    }
}
