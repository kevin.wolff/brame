<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class DataFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i = 0; $i <=3; $i ++)
        {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setPseudo($faker->word);
            $user->setPassword('123456');
            $user->setRoles(['ROLE_USER']);

            $post = new Post();
            $post->setAuthorId($user);
            $post->setMessage($faker->text(100));
            $post->setHashtag($faker->text(15));

            $manager->persist($post);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
