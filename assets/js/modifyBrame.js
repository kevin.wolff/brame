// Variable de la div html 'modifyBrameForm'
const modifyBrameForm = document.getElementById('modifyBrameForm');

// Valeur ID du post selectionne
const postId = modifyBrameForm.dataset.id;

// Fetch des valeurs actuelles du post
fetch(`/api/posts/${postId}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        const messageBrame = document.getElementById('messageBrame');
        messageBrame.innerHTML = result.message;

        const brameHashtag = document.getElementById('brameHashtag');
        brameHashtag.value = result.hashtag;

        // Lorsque le formulaire est submit
        modifyBrameForm.addEventListener('submit', function() {

            const inputMessage = document.getElementById('messageBrame').value;
            const hashtag = document.getElementById('brameHashtag').value;

            // Fetch pour ré-écrire le post en bdd
            fetch(`/api/posts/${postId}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'PUT',

                body:JSON.stringify({message: inputMessage, hashtag: hashtag})
            })
                .then(function(res){
                    window.location.href = '/home';
                })
                .catch(function(res){console.log(res)})
        })

    })
