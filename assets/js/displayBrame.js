import { createViewPost } from "./view/createViewPost";
import { createViewComm } from "./view/createViewComm";

// Variable de la div html 'postDetails'
const postDetails = document.getElementById('postDetails');

// Valeur ID du post selectionne
const postId = postDetails.dataset.id;

// Fetch du post selectionne
fetch(`/api/posts/${postId}`)
    .then((response) => response.json(), )
    .then((result) => {
        createViewPost(result, postDetails)
    })


// Variable de la div html 'postComment'
const postComment = document.getElementById('postComment');

// Fetch des commentaires du post
fetch(`/api/comments?postId=${postId}`)
    .then((response) => response.json(),
    )
    .then((result) => {
        createViewComm(result, postComment)
    });


