import { createViewPost } from "./view/createViewPost";

// Variable de la div html 'lastBrameDisplay'
const brameDisplay = document.getElementById('lastBrameDisplay');

// Fetch des derniers post
fetch('/api/posts')
    .then((response) => response.json(), )
    .then((query) => {

        // Récupère les informations
        const data = query['hydra:member'];

        for (let i = 0; i < data.length; i++) {
            const result = data[i];

            createViewPost(result, brameDisplay);
        }
    })