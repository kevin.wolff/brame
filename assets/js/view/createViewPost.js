// Créer l'affichage d'un seul post

import {likeSystem} from './system/likeSystem.js';
import {commentSystem} from './system/commentSystem.js';
import {deleteSystem} from './system/deleteSystem.js';

function createViewPost(result, FirstHtmlDiv) {

    // Creation de tout les elements html
    const brame = document.createElement('div');
    brame.className = 'py-3 my-2 border-top border-secondary d-flex flex-column';
    FirstHtmlDiv.appendChild(brame);

    const brameHeader = document.createElement('div');
    brameHeader.className = 'd-flex flex-row flex-wrap justify-content-between';

    const headerLeft = document.createElement('div');
    const headerRight = document.createElement('div');
    brameHeader.append(headerLeft, headerRight);

    const brameHashtag = document.createElement('a');
    brameHashtag.className = 'hashPostIndex';
    headerRight.appendChild(brameHashtag);

    const brameAuteur = document.createElement('div');
    brameAuteur.className = 'd-flex flex-row flex-wrap pb-2';
    headerLeft.appendChild(brameAuteur);

    const auteurName = document.createElement('p');
    auteurName.className = 'text-white font-weight-bold py-0 my-0 pr-2';
    brameAuteur.appendChild(auteurName);

    const auteurPseudo = document.createElement('a');
    auteurPseudo.className = 'pseudoPostIndex py-0 my-0 px-0';
    brameAuteur.appendChild(auteurPseudo);

    const createdAt = document.createElement('div');
    headerLeft.appendChild(createdAt);
    const datetime = document.createElement('p');
    datetime.className = 'datePostIndex text-white pb-2 my-0';
    createdAt.appendChild(datetime);

    const brameMessageLink = document.createElement('a');
    const brameMessageDiv = document.createElement('div');
    brameMessageDiv.className = 'messagePostIndex bg-secondary p-2 my-1 shadow-sm rounded text-primary';
    brameMessageLink.appendChild(brameMessageDiv);
    const brameMessage = document.createElement('p');
    brameMessageDiv.appendChild(brameMessage);

    brame.append(brameHeader, brameMessageLink);

    // Valeur hashtag
    brameHashtag.href = '/displayHashtag/' + result.hashtag;
    brameHashtag.innerHTML = '# ' + result.hashtag;

    // Valeur contenu du post (message) dans un container
    brameMessageLink.href = '/displayBrame/' + result.id;
    brameMessage.innerHTML = result.message;

    // Valeur date et heure de création du post
    const date = new Date(result.createdAt);
    const dateOfCreation = ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
    const timeOfCreation = date.getHours() + "h" + date.getMinutes();
    datetime.innerText = dateOfCreation + " - " + timeOfCreation;

    // Fetch des infos du profil
    fetch(`${result.authorId}`)
        .then((response) => response.json(), )
        .then((result) => {

            // Valeur nom prenom et pseudo de l'auteur dans container flex
            auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
            auteurPseudo.href = '/displayProfil/' + result.id;
            auteurPseudo.innerHTML = '@' + result.pseudo;

            if (userId === result.id) {
                buttonDiv.append(modifyBrameButton, deleteBrameButton);
            }
        })

    // Div pour les boutons
    const buttonDiv = document.createElement('div');
    buttonDiv.className = 'd-flex flex-row flex-wrap justify-content-end my-3';
    brame.appendChild(buttonDiv);

    // bouton pour liker
    const likeButton = document.createElement('p');
    likeButton.href = '#';
    likeButton.className = "bouton mr-3 ml-2 shadow d-flex justify-content-center align-items-center";

    const iconLikePath = require('../../images/like.png');
    const iconLikeImg = `<img src="${iconLikePath.default}" alt="like">`;
    const iconLikePath2 = require('../../images/like2.png');
    const iconLikeImg2 = `<img src="${iconLikePath2.default}" alt="like">`;

    likeButton.innerHTML = iconLikeImg2;

    likeButton.addEventListener("mouseover", function (event) {
        event.target.innerHTML = iconLikeImg;
    });
    likeButton.addEventListener("mouseleave", function (event) {
        event.target.innerHTML = iconLikeImg2;
    });

    // likeSystem.js
    let usersLikes = result.usersLikes;

    let likesCounter = document.createElement('p');
    likesCounter.classList = 'likesCounter mt-2 mr-2 text-white';
    likesCounter.innerText = usersLikes.length;

    likeSystem(likeButton, userId, usersLikes, likesCounter, result);

    // Bouton pour commenter - display le formulaire de commentaire
    const commentButton = document.createElement('p');
    commentButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

    const iconComPath = require('../../images/comment.png');
    const iconComImg = `<img src="${iconComPath.default}" alt="comment">`;
    const iconComPath2 = require('../../images/comment2.png');
    const iconComImg2 = `<img src="${iconComPath2.default}" alt="comment">`;

    commentButton.innerHTML = iconComImg2;

    commentButton.addEventListener("mouseover", function (event) {
        event.target.innerHTML = iconComImg;
    });
    commentButton.addEventListener("mouseleave", function (event) {
        event.target.innerHTML = iconComImg2;
    });

    // Formulaire pour commenter
    const commentBrameForm = document.createElement('div');
    commentBrameForm.className = "container commentBrameForm"

    const commentTitle = document.createElement('p');
    commentTitle.className = "h4 text-white";
    commentTitle.innerHTML = "Commenter";

    const commentForm = document.createElement('form');

    const commentBrameLabel = document.createElement('label');
    commentBrameLabel.setAttribute("for", "name");
    commentBrameLabel.className = "text-primary";
    commentBrameLabel.innerHTML = "Votre commentaire";

    const textAreaComment = document.createElement('textarea');
    textAreaComment.setAttribute("name", "commentBrame");
    textAreaComment.setAttribute("id", "commentBrame");
    textAreaComment.setAttribute("rows", "4");
    textAreaComment.setAttribute("maxlength", "140");
    textAreaComment.className = "form-control";

    const minCaractTextArea = document.createElement('p');
    minCaractTextArea.className = "text-primary mt-2";
    minCaractTextArea.innerHTML = "Min 5 caractères";

    const btnFormComment = document.createElement('button');
    btnFormComment.setAttribute("id", "createBrameContent");
    btnFormComment.className = "btn btn-primary mt-3"
    btnFormComment.innerHTML = "Commenter";

    brame.append(commentBrameForm)
    commentBrameForm.append(commentTitle, commentForm);
    commentForm.append(commentBrameLabel, textAreaComment, minCaractTextArea, btnFormComment);

    // Fetch pour récupérer le nombre de commentaires
    let commentsCounter = document.createElement('p');
    commentsCounter.classList = 'likesCounter mt-2 mr-2 text-white';

    fetch(`/api/comments?postId=${result.id}`)
        .then((response) => response.json(), )
        .then((query) => {

            // Récupère les informations
            const data = query['hydra:member'];

            commentsCounter.innerText = data.length;
        })

    // commentSystem.js
    const postId = `${result.id}`;
    commentSystem(commentButton, postId, commentBrameForm, textAreaComment, userId);

    // Bouton pour modifier - redirige sur modifyBrame
    const modifyBrameButton = document.createElement('a');
    modifyBrameButton.href = '/modifyBrame/' + result.id;
    modifyBrameButton.className = "edit bouton mx-2 shadow d-flex justify-content-center align-items-center";

    const iconEdPath = require('../../images/edit.png');
    const iconEdImg = `<img src="${iconEdPath.default}" alt="edit">`;
    const iconEdPath2 = require('../../images/edit2.png');
    const iconEdImg2 = `<img src="${iconEdPath2.default}" alt="edit">`;

    modifyBrameButton.innerHTML = iconEdImg2;

    modifyBrameButton.addEventListener("mouseover", function (event) {
        event.target.innerHTML = iconEdImg;
    });
    modifyBrameButton.addEventListener("mouseleave", function (event) {
        event.target.innerHTML = iconEdImg2;
    });

    // Bouton pour supprimer - fetch l'api pour supprimer le post
    const deleteBrameButton = document.createElement('a');
    deleteBrameButton.href = '#';
    deleteBrameButton.className = "bouton mx-2 shadow d-flex justify-content-center align-items-center";

    const iconSupPath = require('../../images/trash.png');
    const iconSupImg = `<img src="${iconSupPath.default}" alt="delete">`;
    const iconSupPath2 = require('../../images/trash2.png');
    const iconSupImg2 = `<img src="${iconSupPath2.default}" alt="delete">`;

    deleteBrameButton.innerHTML = iconSupImg2;

    deleteBrameButton.addEventListener("mouseover", function (event) {
        event.target.innerHTML = iconSupImg;
    });
    deleteBrameButton.addEventListener("mouseleave", function (event) {
        event.target.innerHTML = iconSupImg2;
    });

    // deleteSystem.js
    deleteBrameButton.addEventListener('click', e => {
        e.preventDefault();
        deleteSystem(result);
    })

    // appends
    buttonDiv.append(likesCounter, likeButton, commentsCounter, commentButton);
}

export { createViewPost };