const deleteSystem = (result) => {

    // Fetch pour supprimer le post
    fetch(`/api/posts/${result.id}`, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'delete'
    })
        .then(function (res) {
            window.location.href = '/home';
        })
        .catch(function (res) {
            console.log(res)
        })
}

export { deleteSystem };