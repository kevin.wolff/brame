const likeSystem = (likeButton, userId, usersLikes, likesCounter, result) => {

	likeButton.addEventListener('click', e => {
		e.preventDefault();

		if (userId === "Not connected") {
			const alertBox = document.getElementById('alertBox');
			alertBox.className = "alertOn";
		}
		else {

			// Test si le user a déjà liké le post
			let alreadyLiked = false;

			for (let i = 0; i < usersLikes.length; ++i) {
				let id = usersLikes[i];

				if (id === ("/api/users/" + userId)) {
					alreadyLiked = true;
				}
			}

			// Si le user n'a pas déjà liké
			if (alreadyLiked === false) {
				usersLikes.push("/api/users/" + userId);

				fetch(`/api/posts/${result.id}`, {
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'put',

						body: JSON.stringify({
							usersLikes: usersLikes
						})
					})
					.then(() => {
						likesCounter.innerText = usersLikes.length;
					})
					.catch(function (res) {
						console.log(res)
					})

			}

			// Si le user a déjà liké
			else if (alreadyLiked) {
				let indexOfId = usersLikes.indexOf("/api/users/" + userId);
				usersLikes.splice(indexOfId, 1);

				fetch(`/api/posts/${result.id}`, {
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'put',
						body: JSON.stringify({
							usersLikes: usersLikes
						})
					})
					.then(() => {
						likesCounter.innerText = usersLikes.length;
					})
					.catch(function (res) {
						console.log(res)
					})
			}
		}
	})
}

export { likeSystem };