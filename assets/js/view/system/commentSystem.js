const commentSystem = (commentButton, postId, commentBrameForm, textAreaComment, userId) => {

    commentButton.addEventListener('click', function () {

        if(userId === 'Not connected') {
            const alertBox = document.getElementById('alertBox');
            alertBox.className = "alertOn";
        }
        else {

            if (commentBrameForm.classList.contains('clicked')) {
                commentBrameForm.classList.remove('clicked');
            } else {
                commentBrameForm.classList.add('clicked');
            }
        }
    })

    // Lorsque le formulaire est submit
    commentBrameForm.addEventListener('submit', function() {

        const commentValue = textAreaComment.value;

        // FETCH POUR ECRIRE LE COMMENTAIRE EN BASE DE DONNEES
        fetch('/api/comments', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',

            body:JSON.stringify({postId: postId, message: commentValue})
        })
            .then(function(res){
                window.location.href = `/displayBrame/${postId}`;
            })
            .catch(function(res){console.log(res)})
    })
}

export { commentSystem };