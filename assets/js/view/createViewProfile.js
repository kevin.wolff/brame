// Créer l'affichage d'un profil

import { createViewPost } from "./createViewPost";

function createViewProfile(result, displayProfil) {

    // User Identity (h1)
    const userIdentity = document.createElement('h1');
    userIdentity.innerText = result.firstName + ' ' + result.lastName;
    userIdentity.className = 'text-primary text-center my-5 font-weight-bold';

    // Section user info
    const userInfo = document.createElement('div');
    userInfo.className = 'my-5';

    // Inside user info
    const titleInfos = document.createElement('h2');
    titleInfos.innerText = 'Profil';
    titleInfos.className = 'h4 text-white mb-3';

    // First name
    const divFname = document.createElement('div');
    divFname.className = "d-flex";

    const fNameLabel = document.createElement('p');
    fNameLabel.className = 'mb-1 ml-5 font-weight-bold userInfoLabel';
    fNameLabel.innerHTML = 'Prénom';
    const fName = document.createElement('p');
    fName.className = 'mb-1 font-weight-bold';
    fName.innerHTML = result.firstName;

    // Last name
    const divLname = document.createElement('div');
    divLname.className = "d-flex";

    const lNameLabel = document.createElement('p');
    lNameLabel.className = 'mb-1 ml-5 font-weight-bold userInfoLabel';
    lNameLabel.innerHTML = 'Nom';
    const lName = document.createElement('p');
    lName.className = 'mb-1 font-weight-bold';
    lName.innerHTML = result.lastName;

    // Pseudo
    const divPseudo = document.createElement('div');
    divPseudo.className = "d-flex";

    const pseudoLabel = document.createElement('p');
    pseudoLabel.className = 'mb-1 ml-5 font-weight-bold userInfoLabel';
    pseudoLabel.innerHTML = 'Pseudo';
    const pseudo = document.createElement('p');
    pseudo.className = 'mb-1 font-weight-bold';
    pseudo.innerHTML = result.pseudo;

    divFname.append(fNameLabel, fName);
    divLname.append(lNameLabel, lName);
    divPseudo.append(pseudoLabel, pseudo);

    userInfo.append(titleInfos, divFname, divLname, divPseudo);

    // Section user brame
    const userBrames = document.createElement('div');
    userBrames.className = 'my-5';

    // Inside user brame
    const titleBrames = document.createElement('h2');
    titleBrames.className = 'h4 text-white mb-3';
    titleBrames.innerText = 'Brames';

    userBrames.append(titleBrames);

    const userComment = result.posts;

    for(let i = 0; i < userComment.length; i++) {
        const result = userComment[i];

        // Fetch pour les post du profil selectionné
        fetch(`${result}`)
            .then((response) => response.json(), // Si la requete est valide
            )
            .then((result) => {
                createViewPost(result, userBrames);
            });
    }
    displayProfil.append(userIdentity, userInfo, userBrames); // userImg , userMentions
}

export { createViewProfile };