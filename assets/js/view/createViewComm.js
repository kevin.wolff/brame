// Créer l'affichage de plusieurs commentaires

function createViewComm(result, postComment) {

    // Récupère les informations
    const data = result['hydra:member'];

    const title = document.createElement('p');
    title.innerText = "Commentaires";
    title.className = "h4 text-white ml-auto"
    postComment.appendChild(title);

    for(let i = 0; i < data.length; i++) {

        const result = data[i];

        // Création de tout les elements HTML
        const lastComment = document.createElement('div');
        lastComment.className = 'lastComment my-4';
        postComment.appendChild(lastComment);

        const commentAuteur = document.createElement('div');
        commentAuteur.className = 'd-flex flex-wrap align-items-center flex-row';
        const auteurName = document.createElement('p');
        auteurName.className = 'text-primary font-weight-bold py-2 my-0 pr-2';
        commentAuteur.appendChild(auteurName);
        const auteurPseudo = document.createElement('a');
        auteurPseudo.className = 'pseudoComment py-2';
        commentAuteur.appendChild(auteurPseudo);

        const brameCommentDiv = document.createElement('div');
        brameCommentDiv.className = 'commentPost bg-secondary p-2 shadow-sm rounded text-primary d-flex flex-sm-row flex-column';
        const brameComment = document.createElement('p');
        brameComment.className = 'flex-grow-1';
        brameCommentDiv.appendChild(brameComment);
        const datetime = document.createElement('p');
        datetime.className = 'dateComment text-primary text-right flex-shrink-0 align-self-end px-2 m-0';
        const date = new Date(result.createdAt);
        brameCommentDiv.appendChild(datetime);

        lastComment.append(commentAuteur, brameCommentDiv);

        // contenu du post (commentaire) dans un container
        brameComment.innerHTML = result.message;

        // date et heure de création du commentaire
        const dateOfCreation = ("0" + date.getDate()).slice(-2)+"/"+("0" + (date.getMonth()+1)).slice(-2)+"/"+date.getFullYear();
        const timeOfCreation = date.getHours()+"h"+date.getMinutes();
        datetime.innerText = dateOfCreation + " - " + timeOfCreation;

        // Fetch les infos du profil
        fetch(`${result.authorId}`)
            .then((response) => response.json(), // Si la requete est valide
            )
            .then((result) => {

                // nom prenom et pseudo de l'auteur dans container flex
                auteurName.innerHTML = `${result.firstName} ${result.lastName}`;
                auteurPseudo.href = '/displayProfil/'+result.id;
                auteurPseudo.innerHTML = '@'+result.pseudo;
            })
    }
}

export { createViewComm };