import { createViewPost } from "./view/createViewPost";

// Variable de la div html 'displayPostUser'
const displayPostUser = document.getElementById('displayPostUser');

// Valeur du profil selectionne
const profilId = displayPostUser.dataset.id;

// Fetch des post du user
fetch(`/api/users/${profilId}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((result) => {

        // Fetch du post selectionne
        fetch(result.posts)
            .then((response) => response.json(), )
            .then((result) => {
                createViewPost(result, displayPostUser)
            })
    });