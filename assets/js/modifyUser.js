// Variable de la div html 'modifyUserForm'
const modifyUserForm = document.getElementById('modifyUserForm');

// Valeur ID du user
const userId = modifyUserForm.dataset.id;

// Fetch des valeurs actuelles du profil
fetch(`/api/users/${userId}`)
    .then((response) => response.json(),
    )
    .then((result) => {

        const modifyUserFirstName = document.getElementById('modifyUserFirstName');
        modifyUserFirstName.value = result.firstName;

        const modifyUserLastName = document.getElementById('modifyUserLastName');
        modifyUserLastName.value = result.lastName;

        const modifyUserPseudo = document.getElementById('modifyUserPseudo');
        modifyUserPseudo.value = result.pseudo;

        const modifyUserEmail = document.getElementById('modifyUserEmail');
        modifyUserEmail.value = result.email;

        // Lorsque le formulaire est submit
        modifyUserForm.addEventListener('submit', function() {

            const inputModifyUserFirstName = document.getElementById('modifyUserFirstName').value;
            const inputModifyUserLastName = document.getElementById('modifyUserLastName').value;
            const inputModifyUserPseudo = document.getElementById('modifyUserPseudo').value;
            const inputModifyUserEmail = document.getElementById('modifyUserEmail').value;

            // Fetch pour ré-écrire le profil en bdd
            fetch(`/api/users/${userId}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'PUT',

                body:JSON.stringify({firstName: inputModifyUserFirstName, lastName: inputModifyUserLastName,
                    pseudo: inputModifyUserPseudo, email: inputModifyUserEmail})
            })
                .then(function(res){
                    window.location.href = '/home';
                })
                .catch(function(res){console.log(res)})
        });
    })


