// Variable du formulaire html 'newBrameForm'
const newBrameForm = document.getElementById('newBrameForm');

// Lorsque le formulaire est submit
newBrameForm.addEventListener('submit', function() {

    const inputMessage = document.getElementById('messageBrame').value;
    const hashtag = document.getElementById('brameHashtag').value;

    // Fetch pour écrire le post en bdd
    fetch('/api/posts', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'POST',

        body:JSON.stringify({message: inputMessage, hashtag: hashtag})
    })
        .then(function(res){
            window.location.href = '/home';
        })
        .catch(function(res){console.log(res)})
})