import {createViewPost} from "./view/createViewPost";

// Variable de la div html 'hashtagValue'
const listHashtags = document.getElementById('listHashtags');

// Valeur du hashtag selectionne
const hashtagQuery = listHashtags.dataset.id;

// Fetch des posts avec le même hashtag
fetch(`/api/posts?hashtag=${hashtagQuery}`)
    .then((response) => response.json(),
    )
    .then((query) => {

        // Récupère les informations
        const data = query['hydra:member'];

        for (let i = 0; i < data.length; i++) {
            const result = data[i];

            createViewPost(result, listHashtags);
        }
    });