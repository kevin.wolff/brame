import { createViewProfile } from "./view/createViewProfile";

// Variable de la div html 'displayProfile'
const displayProfile = document.getElementById('displayProfile');

// Valeur du profil selectionne
const profilId = displayProfile.dataset.id;

// Fetch des infos du profil selectionne
fetch(`/api/users/${profilId}`)
    .then((response) => response.json(), // Si la requete est valide
    )
    .then((result) => {
        createViewProfile(result, displayProfile);
    });